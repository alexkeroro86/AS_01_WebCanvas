# Assignment - Web Canvas by 105062124 <b>(Please use Chrome to open)<b/>
![Main](images/readme-main.PNG)

## Basic components
#### 1. Basic control tools
* Elementary gadgets
    * Brush : It works like a pencil such that you can draw anything on the canvas.<br/>
    ![Brush](images/readme-brush.PNG)
    * Eraser : It is just an eraser that it can erase the things.<br/>
    ![Eraser](images/readme-eraser.PNG)
* Color selector : It will affect the color while drawing circle, rectagnle, and pencil, etc.<br/>
    ![Color selector](images/readme-selector.png)
* Simple menu <b>(brush size)</b> : <br/> It will expand when you click on it, and has a fixed step of 2 to change the size.<br/>
    ![Brush size](images/readme-brush-size.png)

#### 2. Text input
* User can type texts on canvas. In addition, there is no sensored word or limited language.<br/>
    ![Text input](images/readme-text-input.png)
* Font menu <b>(typeface and size)</b> : <br/> There is a datalist of predefined styles that you can choose from and also autocompletes when you type a part of word.
* At last, it is changing the size and style immediately. <br/>
    ![Font menu](images/readme-font-menu.png)

#### 3. Cursor icon
* Icon can change according to the currently used tool. <br/>
    ![Icon change](images/readme-icon-change.gif)

#### 4. Refresh button
* Reset the whole canvas and clear the undo/redo stack. <br/>
    ![Refresh button](images/readme-refresh.png)

## Advanced tools
#### 1. Different brush shapes
* We have circle, rectangle, and triangle to choose from the tool bar. <br/>
    ![Brush shapes](images/readme-brush-shapes.png)

#### 2. Un/Re-do button
* We can track our steps via these buttons, and re-draw the previous step, which will cause it to be the current step. <br/>
    ![Un/re-do buttons](images/readme-unredo.png)

#### 3. Image tool
* It works like the stamp tool, but we can upload our own pictures to draw on the canvas.
* Furthermore, we can change the size by dragging with the mouse. <br/>
    ![Image tool](images/readme-image-tool.gif)

#### 4. Download
* Save the canvas into your local desktop. <br/>
    ![Download button](images/readme-download.png)

## Appearence
#### 1. Tool bar
* There are two types of buttons:
    * One is to draw, like pencil, circle, etc, and it will be selcted unless you click another mode or click the title "WebCanvas." <br/>
    ![Mode btn](images/readme-mode-btn.gif)
    * The other is to control the canvas size or download, etc, and it will blink while you click it. <br/>
    ![System btn](images/readme-system-btn.gif)
* It is responsive. For example, when the screen is small, each line of the toolbar will be scrollable row. <br/>
    ![Responsive bar](images/readme-responsive-bar.gif)

#### 2. Painting area
* If the canvas is so large that the painting area can not handle, it wiil overflow by two-axis scrollbar. <br/>
    ![Responsiv canvas](images/readme-responsive-canvas.gif)

#### 3. Stylized scrollbar
* Change the initial scrollbar into a pretty one, and it will react when the mouse is entering the area and clicking it.

#### 4. Pretty color picker
* It changes the color you picked in realtime. <br/>
    ![Picker change](images/readme-picker-change.gif)

#### 5. Interactive and stylized input field
* In text mode, the input field only shows when you are in that mode, and the same as change font style button, canvas size button.
* Change the text field and button into modern feeling.

#### 6. Stylized down/up-load button
* Discard the default html up/down-load button into a more stylized form.

#### 7. Stylized brush size range bar
* It will change the style when you hover, click, and drag on it. <br/>
    ![Font range](images/readme-size-range.gif)

## Other useful widgets
#### 1. Resize button
* Once you do not feel comfortable to the current size of canvas, you can resize it immediately and also it is redoable.
* Moreovre, one can change the background color of the canvas, and the eraser can work correctly.
* In the end, if you set all the attributes right, click [OK] then it will work now. <br/>
    ![Resize demo](images/readme-resize-demo.gif)

#### 2. Draw with scrollbar moving
* Even if the canvas is scrolled to another side, one can still draw the things on the right place.
    ![Scroll demo](images/readme-scroll-demo.gif)

#### 3. Upload background image
* In addition to resizing or recoloring the canvas, you can edit the images whatever you like on the canvas based on its tools.

#### 4. Additional brush shape
* There is one more brush shape except from the required ones. One can draw oval on the canvas, too.

#### 5. Control to fill or not
* All of the brush shapes allow you to decide whether fill the shapes or not.

#### 6. Free mode
* You can cancel the current mode by clicking the title "WebCanvas." <br/>
    ![A3456 demp](images/readme-A3456-demo.gif)

#### 7. Breathing background animation
* If the canvas is small enough, you can see the background of painting area, which changes the colors like it is breathing.
* Make you feel peacful and focus.

#### 8. Button title snippet
* When you hover on the buttons, they will show their names so that you can clearly know what they are doing. <br/>
    ![Hover title](images/readme-hover-title.png)

#### 9. Keycommand combined
* There are two parts
    * In the toolbar window, the scrolling is locked along the x-axis.
    * In the canvas window, the initial scrolling is vertical. However, if you press on the [ALT] key, it will scroll horizantally.<b>(PS: you might be focused on the canvas, and then it can works functionally)<b/>
* Everytime you click the font style list, it will clean up the list input for you.

#### 10. Display text input in realtime
* With your mouse putting in the canvas, you will see what you type in the text input field right away. <br/>
    ![Text demo](images/readme-text-demo.gif)

#### 11. One more thing
* There would be some changing from the gif since it changes from one version to another one.
