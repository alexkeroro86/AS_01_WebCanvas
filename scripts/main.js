!function() {
    'use strict';

    window.onload = () => {
        init();
    };
    
    var horizantalScrollbars = document.querySelectorAll(".horizantalScrollbar");
    var modeButtons = document.querySelectorAll(".mode");
    var systemButtons = document.querySelectorAll(".system");
    var resizeButton = document.querySelector("#resizeAttrib");
    var sizeButton = document.querySelector("#sizeAttrib");
    var fontButton = document.querySelector("#fontAttrib");
    var colorButton = document.querySelector("#colorAttrib");
    var colorPicker = document.querySelector("#color-picker");
    var backgroundColorButton = document.querySelector("#backgroundColorAttrib");
    var backgroundColorPicker = document.querySelector("#backgroundColor-picker");
    var logoLast = document.querySelector(".logo-last");
    var canvas = document.querySelector("#mainCanvas");
    var ctx = document.querySelector("#mainCanvas").getContext("2d");
    var limitArea = document.querySelector("#limit");
    var sizeBar = document.querySelector("#size-bar");
    var resizeInput = document.querySelector("#resize-input");
    var inputWidth = document.querySelector("#input-width");
    var inputHeight = document.querySelector("#input-height");
    var resizeCheck = document.querySelector("#resize-check");
    var uploadImgButton = document.querySelector("#upload-img");
    var uploadBackgrounfImg = document.querySelector("#upload-bg");
    var textButton = document.querySelector("#textAttrib");
    var textInput = document.querySelector("#text-input");
    var inputWords = document.querySelector("#input-words");
    var fontInput = document.querySelector("#font-input");
    var inputFontSize = document.querySelector("#input-font-size");
    var inputFontStyle = document.querySelector("#input-font-style");
    var strokeStyle = document.querySelector("#stroke-style");
    var strokeI = document.querySelector("#stroke-i");

    var mode = "free";
    var isPainting = false;
    var isMoving = false;
    var lastMouseX = 0.0;
    var lastMouseY = 0.0;
    var curMouseX = 0.0;
    var curMouseY = 0.0;
    var isKeyAlt = false;
    var canvasBakcgroundColor = "rgb(255,250,250)";
    var canvasStack = [];
    var canvasStep = 0;
    var isLeavingCanvas = false;
    var preCanvasImg = new Image();
    var uploadImg = new Image();
    var reader = new FileReader();
    var words = "";
    var fontSize = 20;
    var fontStyle = "Georgia";
    var strokeFill = true;

    function init() {
        for(let i = 0; i < horizantalScrollbars.length; ++i) {
            horizantalScrollbars[i].addEventListener("wheel", function(){
                handleMenuHorizantalScrollbar(event, horizantalScrollbars[i]);
            });
        }
        for(let i = 0; i < modeButtons.length; ++i) {
            modeButtons[i].addEventListener("click", function(){
                handleModeButton(modeButtons[i]);
            });
        }
        for(let i = 0; i < systemButtons.length; ++i) {
            systemButtons[i].addEventListener("click", function(){
                handleSystemButton(systemButtons[i]);
            });
        }
        colorPicker.addEventListener("change", function() {
            handleColorPicker();
        });
        backgroundColorPicker.addEventListener("change", function() {
            handleBackgroundColorPicker();
        });
        sizeButton.addEventListener("click", function() {
            handleSizeButton(event);
        });
        sizeBar.addEventListener("change", function() {
            handleSizeBar();
        });
        resizeButton.addEventListener("click", function() {
            handleResizeButton();
        });
        resizeCheck.addEventListener("click", function() {
            handleResizeCheck();
        });
        textButton.addEventListener("click", function() {
            handleTextButton();
        });
        inputWords.addEventListener("input", function() {
            handleInputWords();
        });
        fontButton.addEventListener("click", function() {
            handleFontButton();
        });
        inputFontSize.addEventListener("input", function() {
            handleFontSize();
        });
        inputFontStyle.addEventListener("input", function() {
            handleFontStyle();
        });

        logoLast.addEventListener("click", function(){
            handleLogoLast();
        });
        strokeStyle.addEventListener("click", function() {
            handleStrokeStyle();
        });

        canvas.addEventListener("mousedown", function() {
            handleMouseDown(event, canvas);
        });
        canvas.addEventListener("mouseleave", function() {
            handleMouseLeave(event);
        });
        canvas.addEventListener("mouseenter", function() {
            handleMouseEnter(event);
        });
        window.addEventListener("mouseup", function() {
            handleMouseUp(event);
        });
        canvas.addEventListener("mousemove", function() {
            handleMouseMove(event, canvas);
        });

        window.addEventListener("keydown", function() {
            handleKeyDown(event);
        });
        window.addEventListener("keyup", function() {
            handleKeyUp(event);
        });
        limitArea.addEventListener("wheel", function() {
            handleCanvasHorizantalScrollbar(event, limitArea);
        });

        uploadImgButton.addEventListener("change", function() {
            handleUploadImageButton(event);
        });
        uploadBackgrounfImg.addEventListener("change", function() {
            handleUploadBackgrounfImgBtn(event);
        });

        ctx.lineWidth = sizeBar.value;
        ctx.fillStyle = canvasBakcgroundColor;
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

        canvasStack.push(document.querySelector("#mainCanvas").toDataURL());
    }

    function handleMenuHorizantalScrollbar(event, element) {
        event.preventDefault();
        let delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
        element.scrollLeft -= (delta*40);
    }
    function handleModeButton(element) {
        for(let i = 0; i < modeButtons.length; ++i) {
            modeButtons[i].classList.remove('selected');
        }
        element.classList.add("selected");
        mode = element.title;
        canvas.style.cursor = `url('./images/${mode}.png') 0 64, auto`;

        if(mode !== "text") {
            textInput.style.display = "none";
        }
    }
    function handleSystemButton(element) {
        element.classList.add("selected");
        setTimeout(() => {element.classList.remove("selected");}, 200);
        if(element.title === "reset") {
            ctx.fillStyle = canvasBakcgroundColor;
            ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

            canvasStack.length = 0;
            canvasStep = 0;
            canvasStack.push(document.querySelector("#mainCanvas").toDataURL());
        }
        else if(element.title === "undo") {
            canvasUndo();
        }
        else if(element.title === "redo") {
            canvasRedo();
        }
        else if(element.title === "download") {
            element.href = canvas.toDataURL();
        }
    }
    function handleColorPicker() {
        colorButton.style.color = colorPicker.value;
    }
    function handleBackgroundColorPicker() {
        backgroundColorButton.style.color = backgroundColorPicker.value;
        canvasBakcgroundColor = backgroundColorPicker.value;
    }
    function handleSizeButton(event) {
        sizeBar.style.display = (sizeBar.style.display === 'none' || sizeBar.style.display == 0) ? "inline-block" : "none";
    }
    function handleSizeBar() {
        ctx.lineWidth = sizeBar.value;
    }
    function handleResizeButton() {
        resizeInput.style.display = (resizeInput.style.display === 'none' || resizeInput.style.display == 0) ? "inline-block" : "none";
    }
    function handleResizeCheck() {
        canvas.width = inputWidth.value;
        canvas.height = inputHeight.value;
        ctx.lineWidth = sizeBar.value;
        ctx.fillStyle = canvasBakcgroundColor;
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        canvasKeepTrack();
    }
    function handleTextButton() {
        textInput.style.display = (textInput.style.display === 'none' || textInput.style.display == 0) ? "inline-block" : "none";
    }
    function handleInputWords() {
        words = inputWords.value;
    }
    function handleFontButton() {
        fontInput.style.display = (fontInput.style.display === 'none' || fontInput.style.display == 0) ? "inline-block" : "none";
    }
    function handleFontSize() {
        fontSize = inputFontSize.value;
    }
    function handleFontStyle() {
        fontStyle = inputFontStyle.value;
    }

    function handleLogoLast() {
        for(let i = 0; i < modeButtons.length; ++i) {
            modeButtons[i].classList.remove('selected');
        }
        mode = "free";
        canvas.style.cursor = "auto";
    }
    function handleStrokeStyle() {
        strokeI.className = (strokeI.className === "far fa-bookmark") ? "fas fa-bookmark" : "far fa-bookmark";
        strokeFill = (strokeI.className === "far fa-bookmark") ? false : true;
    }

    function handleMouseDown(event, element) {
        lastMouseX = event.pageX - element.offsetLeft + limitArea.scrollLeft;
        lastMouseY = event.pageY - element.offsetTop + limitArea.scrollTop;
        if(mode === 'pencil' || mode === 'eraser') {
            isPainting = true;
            ctx.beginPath();
            ctx.moveTo(lastMouseX, lastMouseY);
        }
        else if(mode === "rectangle" || mode === "circle" || mode === "oval" || mode === "triangle" || mode === "upload") {
            isPainting = true;
            preCanvasImg.src = document.querySelector("#mainCanvas").toDataURL();
        }
    }
    function handleMouseUp(event) {
        if(mode === 'pencil' || mode === 'eraser') {
            isPainting = false;
            ctx.closePath();
            console.log("mouse up!!");

        }
        else if(mode === "rectangle" || mode === "circle" || mode === "oval" || mode === "triangle" || mode === "upload") {
            isPainting = false;
            if(isMoving) {
                ctx.fillStyle = colorPicker.value;
                if(mode === "rectangle") {
                    if(strokeFill)
                        ctx.fillRect(lastMouseX, lastMouseY, curMouseX - lastMouseX, curMouseY - lastMouseY);
                    ctx.closePath();
                    console.log("Complete rectangle!");
                }
                else if(mode === "circle") {
                    if(strokeFill)
                        ctx.fill();
                    ctx.closePath();
                    console.log("Complete circle!");
                }
                else if(mode === "oval") {
                    if(strokeFill)
                        ctx.fill();
                    ctx.closePath();
                    console.log("Complete oval!");
                }
                else if(mode === "triangle") {
                    if(strokeFill)
                        ctx.fill();
                    ctx.closePath();
                    console.log("Complete triangle!");
                }
                else if(mode === "upload") {
                    console.log("Complete image!");
                }
            }
            isMoving = false;
        }
        if(mode === "text" && !isLeavingCanvas) {
            ctx.fillStyle = colorPicker.value;
            ctx.fillText(words, curMouseX,curMouseY);
        }
        if(mode !== "free" && !isLeavingCanvas && !isMoving) {
            canvasKeepTrack();
        }
    }
    function handleMouseLeave(event) {
        if(mode === 'pencil' || mode == 'eraser') {
            isPainting = false;
            ctx.closePath();
        }
        else if(mode === "text") {
            let preTextImg = new Image();
            preTextImg.src = canvasStack[canvasStep];
            ctx.drawImage(preTextImg, 0, 0);
        }
        isLeavingCanvas = true;
    }
    function handleMouseEnter(event) {
        isLeavingCanvas = false;
    }
    function handleMouseMove(event, element) {
        curMouseX = event.pageX - element.offsetLeft + limitArea.scrollLeft;
        curMouseY = event.pageY - element.offsetTop + limitArea.scrollTop;
        if(isPainting) {
            if(mode === 'pencil' || mode === 'eraser') {
                ctx.strokeStyle = (mode === "pencil") ? colorPicker.value : canvasBakcgroundColor;
                ctx.lineWidth = sizeBar.value;
                ctx.lineJoin = "round";
                ctx.lineTo(curMouseX, curMouseY);
                ctx.stroke();
            }
            else if(mode === "rectangle") {
                isMoving = true;
                //preCanvasImg.onload = function() {
                    ctx.drawImage(preCanvasImg, 0, 0);
                //};
                ctx.beginPath();
                let rectWidth = curMouseX - lastMouseX;
                let rectHeight = curMouseY - lastMouseY;
                ctx.rect(lastMouseX, lastMouseY, rectWidth, rectHeight);
                ctx.strokeStyle = colorPicker.value;
                ctx.lineJoin = "round";
                ctx.lineWidth = 10;
                ctx.stroke();
            }
            else if(mode === "circle") {
                isMoving = true;
                ctx.drawImage(preCanvasImg, 0, 0);

                ctx.beginPath();
                let circleCenterX = (curMouseX + lastMouseX) * 0.5;
                let circleCenterY = (curMouseY + lastMouseY) * 0.5;
                let circleRadius = Math.sqrt(Math.pow((circleCenterX - curMouseX), 2) + Math.pow((circleCenterY - curMouseY), 2));
                ctx.arc(circleCenterX, circleCenterY, circleRadius, 0, 2 * Math.PI);
                ctx.strokeStyle = colorPicker.value;
                ctx.lineJoin = "round";
                ctx.lineWidth = 10;
                ctx.stroke();
                console.log(`Processing circlr: (${circleCenterX}, ${circleCenterY}) => radius: ${circleRadius}`);
            }
            else if(mode === "oval") {
                isMoving = true;
                ctx.drawImage(preCanvasImg, 0, 0);

                let radiusX = (curMouseX - lastMouseX) * 0.5,
                    radiusY = (curMouseY - lastMouseY) * 0.5,
                    centerX = lastMouseX + radiusX,
                    centerY = lastMouseY + radiusY,
                    step = 0.01,
                    angle = step,
                    pi2 = Math.PI * 2 + step;
            
                ctx.beginPath();
                ctx.moveTo(centerX + radiusX * Math.cos(0),
                        centerY + radiusY * Math.sin(0));

                for(; angle < pi2; angle += step) {
                    ctx.lineTo(centerX + radiusX * Math.cos(angle), centerY + radiusY * Math.sin(angle));
                }
                
                ctx.strokeStyle = colorPicker.value;
                ctx.lineJoin = "round";
                ctx.lineWidth = 10;
                ctx.stroke();
            }
            else if(mode === "triangle") {
                isMoving = true;
                ctx.drawImage(preCanvasImg, 0, 0);

                ctx.beginPath();
                let triangleSize = Math.sqrt(Math.pow((curMouseX - lastMouseX), 2) + Math.pow((curMouseY - lastMouseY), 2));
                ctx.moveTo(lastMouseX, lastMouseY);
                ctx.lineTo(lastMouseX - triangleSize * 0.5, lastMouseY + triangleSize);
                ctx.lineTo(lastMouseX + triangleSize * 0.5, lastMouseY + triangleSize);
                ctx.lineTo(lastMouseX, lastMouseY);
                ctx.lineTo(lastMouseX - triangleSize * 0.5, lastMouseY + triangleSize);

                ctx.strokeStyle = colorPicker.value;
                ctx.lineJoin = "round";
                ctx.lineWidth = 10;
                ctx.stroke();
            }
            else if(mode === "upload") {
                isMoving = true;
                ctx.drawImage(preCanvasImg, 0, 0);

                let ratio = Math.sqrt(Math.pow((curMouseX - lastMouseX), 2) + Math.pow((curMouseY - lastMouseY), 2)) * 0.005;
                console.log(ratio);
                ctx.drawImage(uploadImg, lastMouseX, lastMouseY, uploadImg.width * ratio, uploadImg.height * ratio);
            }
        }
        if(mode === "text") {
            let preTextImg = new Image();
            preTextImg.src = canvasStack[canvasStep];
            ctx.drawImage(preTextImg, 0, 0);

            ctx.font = `${fontSize}px ${fontStyle}`;
            ctx.fillStyle = colorPicker.value;
            ctx.fillText(words, curMouseX,curMouseY);
        }
    }
    function handleUploadImageButton(event) {
        reader.onload = function(task) {
            // uploadImg.onload = function() {
            //     canvas.width = uploadImg.width;
            //     canvas.height = uploadImg.height;
            //     ctx.drawImage(uploadImg,0,0);
            // };
            uploadImg.src = task.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    }
    function handleUploadBackgrounfImgBtn(event) {
        let tmpReader = new FileReader();
        let tmpImg = new Image();
        tmpReader.onload = function(task) {
            tmpImg.onload = function() {
                canvas.width = tmpImg.width;
                canvas.height = tmpImg.height;
                ctx.drawImage(tmpImg,0,0);
                canvasKeepTrack();
            };
            tmpImg.src = task.target.result;
        };
        tmpReader.readAsDataURL(event.target.files[0]);
    }

    function handleKeyDown(event) {
        let key = event.keyCode;
        console.log("Who are pressed (keyCode): " + key);
        if(key === 18) {
            isKeyAlt = true;
        }
    }
    function handleKeyUp(event) {
        let key = event.keyCode;
        console.log("Who are released (keyCode): " + key);
        if(key === 18) {
            isKeyAlt = false;
        }
    }
    function handleCanvasHorizantalScrollbar(event, element) {
        if(isKeyAlt) {
            event.preventDefault();
            let delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
            element.scrollLeft -= (delta*40);
        }
        else {
            console.log("scrolling vertically!");
        }
    }

    function canvasKeepTrack() {
        ++canvasStep;
        console.log(`canvas keep track: ${canvasStep} => canvasStack load: ${canvasStack.length}`);
        if(canvasStep < canvasStack.length) {
            canvasStep = canvasStack.length;
        }
        canvasStack.push(document.querySelector("#mainCanvas").toDataURL());
    }
    function canvasUndo() {
        if(canvasStep > 0) {
            let canvasUndoImg = new Image();
            canvasUndoImg.src = canvasStack[--canvasStep];
            canvasUndoImg.onload = function() {
                canvas.width = canvasUndoImg.width;
                canvas.height = canvasUndoImg.height;
                ctx.drawImage(canvasUndoImg, 0, 0);
            };
            console.log(`canvas undo: ${canvasStep}`);
        }
    }
    function canvasRedo() {
        if(canvasStep < canvasStack.length-1) {
            let canvasRedoImg = new Image();
            canvasRedoImg.src = canvasStack[++canvasStep];
            canvasRedoImg.onload = function() {
                canvas.width = canvasRedoImg.width;
                canvas.height = canvasRedoImg.height;
                ctx.drawImage(canvasRedoImg, 0, 0);
            };
            console.log(`canvas redo: ${canvasStep}`);
        }
    }
}();